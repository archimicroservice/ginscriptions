package com.tpSI.gInscription.ServiceImplementations;

import com.tpSI.gInscription.Entities.Etudiant;
import com.tpSI.gInscription.Entities.Inscription;
import com.tpSI.gInscription.Repositories.InscriptionRepository;
import com.tpSI.gInscription.Services.InscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.time.Year;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

@Service
@Transactional
public class InscriptionImplementation implements InscriptionService {
    @Autowired
    public InscriptionImplementation(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
    @Autowired
    InscriptionRepository inscriptionRepository;
    Map<String, String> params = new HashMap<>();
    public String urlNotif = "http://localhost:8771/notification/mail/send";
    public String urlE = "http://localhost:8771/getudiants/etudiant";

    private final RestTemplate restTemplate;

    public String genererMatricule() {
        Random random = new Random();
        int chiffreAleatoire = random.nextInt(900) + 100;
        String anneeEnCours = String.valueOf(Year.now().getValue());
        return anneeEnCours + "i" + chiffreAleatoire;
    }
    @Override
    public Inscription saveInscription(Inscription inscription) {

        Etudiant etudiant = restTemplate.getForObject(urlE+"/select/"+inscription.getCodeCandidat(), Etudiant.class);
        inscription.setMatricule(genererMatricule());
        inscription.setCodeCandidat(etudiant.getCodeCandidat());

        params.put("body", "Votre compte etudiant vient d'etre cree avec succes avec pour matricule + " + inscription.getMatricule());
        params.put("subject", "Inscription du candidat");

        System.out.println(params);
        System.out.println(urlNotif+"/"+etudiant.getEmail()+"?body="+params.get("body")+"&subject="+params.get("subject"));
        restTemplate.getForObject(urlNotif+"/"+etudiant.getEmail()+"?body="+params.get("body")+"&subject="+params.get("subject"), String.class);

        return inscriptionRepository.save(inscription);
    }

    @Override
    public Inscription findInscription(Long idInscription) {

        return inscriptionRepository.findById(idInscription).get();
    }

    @Override
    public Inscription updateInscription(Long idInscription, Inscription inscription) {
        return null;
    }

    @Override
    public Inscription deleteInscription(Long idInscription) {
        Inscription inscription1 = inscriptionRepository.findById(idInscription).get();
        inscription1.setSupprimeInscription(true);
        return inscriptionRepository.save(inscription1);
    }

    @Override
    public List<Inscription> listInscription() {
        return inscriptionRepository.findAll();
    }

    @Override
    public List<Inscription> listInscriptionNonSupprime() {
        return null;
    }
}
