package com.tpSI.gInscription.Repositories;

import com.tpSI.gInscription.Entities.Inscription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InscriptionRepository extends JpaRepository<Inscription,Long> {
    public Inscription findByCodeCandidat(String codeCandidat);
    public Boolean existsByCodeCandidat(String codeCandidat);

}
