package com.tpSI.gInscription.Entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
//@RequiredArgsConstructor
@Data
@Entity
@Table(name = "Inscription")
public class Inscription {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long code ;
    private String codeCandidat;

    private String matricule;
    private String filiere ;

    private int paye;

    private boolean supprimeInscription=false;
}
