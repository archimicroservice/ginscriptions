package com.tpSI.gInscription.Entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;


        import lombok.*;
        import javax.persistence.Entity;
        import javax.persistence.*;
        import javax.persistence.GeneratedValue;
        import javax.persistence.Table;
        import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
//@RequiredArgsConstructor
@Data
public class Etudiant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long code;

    private String codeCandidat;

    private String nom;
    private String prenom ;
    private String email ;


    private Date dateNaissance;

    private boolean supprimeEtudiant=false;
}
