package com.tpSI.gInscription.Controllers;

import com.tpSI.gInscription.Entities.Inscription;
import com.tpSI.gInscription.Repositories.InscriptionRepository;
import com.tpSI.gInscription.Services.InscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("/inscription")
@CrossOrigin("*")
public class InscriptionController {
    private final RestTemplate restTemplate;
    @Autowired
    public InscriptionController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Autowired
    private InscriptionService inscriptionService;

    @Autowired
    private InscriptionRepository inscriptionRepository;

    @PostMapping("/save")
    public Inscription saveInscription(@RequestBody Inscription inscription) {
        if (inscriptionRepository.existsByCodeCandidat(inscription.getCodeCandidat()))
            return null;
        return inscriptionService.saveInscription(inscription);
    }
    @GetMapping("/list")
    public List<Inscription> listInscription(){
        return inscriptionService.listInscription();
    }
    @GetMapping("/list/nonSupprime")
    public List<Inscription> listInscriptionNonSupprime(){
        return inscriptionService.listInscriptionNonSupprime();
    }
    @GetMapping("{idInscription}")
    public Inscription findInscription(@PathVariable("idInscription")Long idInscription){
        return inscriptionService.findInscription(idInscription);
    }
    @PutMapping("update/{idInscription}")
    public Inscription updateInscription(@RequestBody Inscription inscription,@PathVariable("idInscription") Long idInscription){
        return inscriptionService.updateInscription(idInscription,inscription);
    }
    @PutMapping("delete/{idInscription}")
    public Inscription deleteInscription(@PathVariable("idInscription") Long idInscription){
        return inscriptionService.deleteInscription(idInscription);
    }

    @DeleteMapping("delete/def/{idInscription}")
    public String deletedefInscription(@PathVariable("idInscription") Long idInscription){
        inscriptionRepository.deleteById(idInscription);
        return "Personne supprime";
    }
    @GetMapping("/exist/{codeCandidat}")
    public Boolean estInscrit(@PathVariable("codeCandidat")String codeCandidat){
        return  (inscriptionRepository.existsByCodeCandidat(codeCandidat));
    }

    @GetMapping("/select/{codeCandidat}")
    public Inscription selectInscription(@PathVariable("codeCandidat")String codeCandidat){
        return  (inscriptionRepository.findByCodeCandidat(codeCandidat));
    }
//    @PutMapping("/inscrire/{codeCandidat}/{newPaye}")
//    public Inscription inscrire(@PathVariable("codeCandidat")String codeCandidat,@PathVariable("newPaye") int newPaye){
//        Inscription inscription = new Inscription();
//        if(estInscrit(codeCandidat)){
//            inscription = inscriptionRepository.findById(codeCandidat).get();
//            inscription.setPaye(inscription.getPaye()+newPaye);
//            return updateInscription(inscription,codeCandidat);
//        }
//        inscription.setCodeCandidat(codeCandidat);
//        inscription.setMatricule(codeCandidat+'M'+codeCandidat);
//        return  saveInscription(inscription);
//    }
}
