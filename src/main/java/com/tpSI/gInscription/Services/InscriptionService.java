package com.tpSI.gInscription.Services;

import com.tpSI.gInscription.Entities.Inscription;

import java.util.List;

public interface InscriptionService {
    //    public Inscription create ();
    Inscription saveInscription(Inscription inscription);
    Inscription findInscription(Long idInscription);
    Inscription updateInscription(Long idInscription,Inscription inscription);
    Inscription deleteInscription(Long idInscription);
    List<Inscription> listInscription();
    List<Inscription> listInscriptionNonSupprime();
}